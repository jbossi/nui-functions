import { Request, Response } from 'express';

const logger = function(req: Request, res: Response, next: () => void) {
	console.log('Request');
	console.log(req.body);
	console.log('Response');
	console.log(res.statusCode);
	next();
};

export default logger;
