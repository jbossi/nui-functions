import { Request, Response } from 'express';

const fbadmin = require('firebase-admin');

const authentication = async function(
	req: Request,
	res: Response,
	next: () => void
) {
	console.log('Check if request is authorized with Firebase ID token');
	let idToken;
	if (
		!req.headers.authorization ||
		!req.headers.authorization.startsWith('Bearer ')
	) {
		console.error(
			'No Firebase ID token was passed as a Bearer token in the Authorization header.'
		);
		res.status(403).send('Unauthorized');
		return;
	} else {
		idToken = req.headers.authorization.split('Bearer ')[1];
	}

	try {
		const decodedIdToken = await fbadmin.auth().verifyIdToken(idToken);
		res.locals.token = decodedIdToken;
		console.log(decodedIdToken);
		next();
		return;
	} catch (error) {
		console.error('Error while verifying Firebase ID token:', error);
		res.status(403).send('Unauthorized');
		return;
	}
};

export default authentication;
