import * as functions from 'firebase-functions';
import logger from '../tools/logger';
import authentication from '../tools/authentication';
import { Request, Response } from 'express';
import express = require('express');
import cors = require('cors');

const fbadmin = require('firebase-admin');

const isValidCode = async function(code: string): Promise<string> {
	return new Promise(async (resolve, reject) => {
		await fbadmin
			.database()
			.ref('/companies')
			.once('value')
			.then(function(snapshot: any) {
				console.log(snapshot.val());
				snapshot.forEach((company: any) => {
					console.log(company.val());
					console.log(company.val().code);
					if (company.val().code === code) {
						console.log(company);
						resolve('companie example');
					}
				});
				reject('Error');
			});
	});
};

const handleValidateToken = async function(
	req: Request,
	res: Response,
	next: () => void
) {
	try {
		if (await isValidCode(req.body.code)) {
			fbadmin
				.database()
				.ref('/users/' + res.locals.token.user_id + '/remoteConfig')
				.update({
					featureFlags: {
						informationAssistant: true,
						inviteFamily: true
					}
				});
			res.status(200).send('Great success');
		} else {
			res.status(400).send('Code not found');
		}
	} catch (e) {
		res.status(400).send('Code not found e');
	}
};

const app = express();
// Automatically allow cross-origin requests
app.use(cors({ origin: true }));

// Add middleware to log request
app.use(logger);

// All requests to the payment API have to be authenticated
app.use(authentication);

app.post('/validateToken', handleValidateToken);

const paymentApp = functions.https.onRequest(app);
export default paymentApp;
