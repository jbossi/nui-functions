import * as functions from 'firebase-functions';
const request = require('request');

const sendSlackNotificationFunc = functions.auth.user().onCreate(user => {
	const number = user.phoneNumber; // The display name of the user.
	const msg = 'Hey a new user ' + number + ' registered';
	const options = {
		method: 'POST',
		url:
			'https://hooks.slack.com/services/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',
		headers: {
			Connection: 'keep-alive',
			'Accept-Encoding': 'gzip, deflate',
			'Content-Type': 'application/json',
			Host: 'hooks.slack.com'
		},
		body: { text: msg },
		json: true
	};

	request(options, function(
		error: Error,
		res: { statusCode: any },
		body: any
	) {
		if (error) {
			console.error(error);
			return;
		}
		console.log(`statusCode: ${res.statusCode}`);
	});
});

export default sendSlackNotificationFunc