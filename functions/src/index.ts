import sendSlackNotificationFunc from './integrations/sendSlackNotification';
import payment from './payment/index';

const fbadmin = require('firebase-admin');
fbadmin.initializeApp({ databaseURL: 'https://nui-dev.firebaseio.com/' });

exports.sendSlackNotification = sendSlackNotificationFunc;
exports.payment = payment;
